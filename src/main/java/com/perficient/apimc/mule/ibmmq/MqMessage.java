package com.perficient.apimc.mule.ibmmq;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;

public class MqMessage {
	
	private String jMSMessageClass;
	private String jMSType;
	private Integer jMSDeliveryMode;
	private Integer jMSDeliveryDelay;
	private Long jMSDeliveryTime;
	private Long jMSExpiration;
	private Integer jMSPriority;
	private String jMSMessageID;
	private Long jMSTimestamp;
	private String jMSCorrelationID;
	private Destination jMSDestination;
	private Destination jMSReplyTo;
	private Boolean jMSRedelivered;
	private String jMSXAppID;
	private Integer jMXSDeliveryCount;
	private Integer jMXSUserID;
	private String jMS_IBM_Character_Set;
	private String jMS_IBM_Encoding;
	private String jMS_IBM_MQSTR;
	private Integer jMS_IBM_MsgType;
	private Integer jMS_IBM_PutApplType;
	private Integer jMS_IBM_PutDate;
	private Integer jMS_IBM_PutTime;
	private String mM_MESSAGE_CONTENT_TYPE;
	private String mM_MESSAGE_ENCODING;
	private String messageBody;
	
	public MqMessage() {}
	
	public MqMessage(Message msg) throws JMSException {
		
		this.jMSMessageClass = msg.getClass().toString();
		this.jMSType = msg.getJMSType();
		this.jMSDeliveryMode = msg.getJMSDeliveryMode();
		this.jMSDeliveryDelay = 0;
		this.jMSDeliveryTime = msg.getJMSDeliveryTime();
		this.jMSExpiration = msg.getJMSExpiration();
		this.jMSPriority = msg.getJMSPriority();
		this.jMSMessageID = msg.getJMSMessageID();
		this.jMSTimestamp = msg.getJMSTimestamp();
		this.jMSCorrelationID = msg.getJMSCorrelationID();
		this.jMSDestination = msg.getJMSDestination();
		this.jMSReplyTo = msg.getJMSReplyTo();
		this.jMSRedelivered = msg.getJMSRedelivered();
		this.jMSXAppID = msg.getObjectProperty("JMSXAppID") == null ? null : msg.getObjectProperty("JMSXAppID").toString();
		
		
		
	}
	
	public String getjMSMessageClass() {
		return jMSMessageClass;
	}
	public void setjMSMessageClass(String jMSMessageClass) {
		this.jMSMessageClass = jMSMessageClass;
	}
	public String getjMSType() {
		return jMSType;
	}
	public void setjMSType(String jMSType) {
		this.jMSType = jMSType;
	}
	public Integer getjMSDeliveryMode() {
		return jMSDeliveryMode;
	}
	public void setjMSDeliveryMode(Integer jMSDeliveryMode) {
		this.jMSDeliveryMode = jMSDeliveryMode;
	}
	public Integer getjMSDeliveryDelay() {
		return jMSDeliveryDelay;
	}
	public void setjMSDeliveryDelay(Integer jMSDeliveryDelay) {
		this.jMSDeliveryDelay = jMSDeliveryDelay;
	}
	public Long getjMSDeliveryTime() {
		return jMSDeliveryTime;
	}
	public void setjMSDeliveryTime(Long jMSDeliveryTime) {
		this.jMSDeliveryTime = jMSDeliveryTime;
	}
	public Long getjMSExpiration() {
		return jMSExpiration;
	}
	public void setjMSExpiration(Long jMSExpiration) {
		this.jMSExpiration = jMSExpiration;
	}
	public Integer getjMSPriority() {
		return jMSPriority;
	}
	public void setjMSPriority(Integer jMSPriority) {
		this.jMSPriority = jMSPriority;
	}
	public String getjMSMessageID() {
		return jMSMessageID;
	}
	public void setjMSMessageID(String jMSMessageID) {
		this.jMSMessageID = jMSMessageID;
	}
	public Long getjMSTimestamp() {
		return jMSTimestamp;
	}
	public void setjMSTimestamp(Long jMSTimestamp) {
		this.jMSTimestamp = jMSTimestamp;
	}
	public String getjMSCorrelationID() {
		return jMSCorrelationID;
	}
	public void setjMSCorrelationID(String jMSCorrelationID) {
		this.jMSCorrelationID = jMSCorrelationID;
	}
	public Destination getjMSDestination() {
		return jMSDestination;
	}
	public void setjMSDestination(Destination jMSDestination) {
		this.jMSDestination = jMSDestination;
	}
	public Destination getjMSReplyTo() {
		return jMSReplyTo;
	}
	public void setjMSReplyTo(Destination jMSReplyTo) {
		this.jMSReplyTo = jMSReplyTo;
	}
	public Boolean getjMSRedelivered() {
		return jMSRedelivered;
	}
	public void setjMSRedelivered(Boolean jMSRedelivered) {
		this.jMSRedelivered = jMSRedelivered;
	}
	public String getjMSXAppID() {
		return jMSXAppID;
	}
	public void setjMSXAppID(String jMSXAppID) {
		this.jMSXAppID = jMSXAppID;
	}
	public Integer getjMXSDeliveryCount() {
		return jMXSDeliveryCount;
	}
	public void setjMXSDeliveryCount(Integer jMXSDeliveryCount) {
		this.jMXSDeliveryCount = jMXSDeliveryCount;
	}
	public Integer getjMXSUserID() {
		return jMXSUserID;
	}
	public void setjMXSUserID(Integer jMXSUserID) {
		this.jMXSUserID = jMXSUserID;
	}
	public String getjMS_IBM_Character_Set() {
		return jMS_IBM_Character_Set;
	}
	public void setjMS_IBM_Character_Set(String jMS_IBM_Character_Set) {
		this.jMS_IBM_Character_Set = jMS_IBM_Character_Set;
	}
	public String getjMS_IBM_Encoding() {
		return jMS_IBM_Encoding;
	}
	public void setjMS_IBM_Encoding(String jMS_IBM_Encoding) {
		this.jMS_IBM_Encoding = jMS_IBM_Encoding;
	}
	public String getjMS_IBM_MQSTR() {
		return jMS_IBM_MQSTR;
	}
	public void setjMS_IBM_MQSTR(String jMS_IBM_MQSTR) {
		this.jMS_IBM_MQSTR = jMS_IBM_MQSTR;
	}
	public Integer getjMS_IBM_MsgType() {
		return jMS_IBM_MsgType;
	}
	public void setjMS_IBM_MsgType(Integer jMS_IBM_MsgType) {
		this.jMS_IBM_MsgType = jMS_IBM_MsgType;
	}
	public Integer getjMS_IBM_PutApplType() {
		return jMS_IBM_PutApplType;
	}
	public void setjMS_IBM_PutApplType(Integer jMS_IBM_PutApplType) {
		this.jMS_IBM_PutApplType = jMS_IBM_PutApplType;
	}
	public Integer getjMS_IBM_PutDate() {
		return jMS_IBM_PutDate;
	}
	public void setjMS_IBM_PutDate(Integer jMS_IBM_PutDate) {
		this.jMS_IBM_PutDate = jMS_IBM_PutDate;
	}
	public Integer getjMS_IBM_PutTime() {
		return jMS_IBM_PutTime;
	}
	public void setjMS_IBM_PutTime(Integer jMS_IBM_PutTime) {
		this.jMS_IBM_PutTime = jMS_IBM_PutTime;
	}
	public String getmM_MESSAGE_CONTENT_TYPE() {
		return mM_MESSAGE_CONTENT_TYPE;
	}
	public void setmM_MESSAGE_CONTENT_TYPE(String mM_MESSAGE_CONTENT_TYPE) {
		this.mM_MESSAGE_CONTENT_TYPE = mM_MESSAGE_CONTENT_TYPE;
	}
	public String getmM_MESSAGE_ENCODING() {
		return mM_MESSAGE_ENCODING;
	}
	public void setmM_MESSAGE_ENCODING(String mM_MESSAGE_ENCODING) {
		this.mM_MESSAGE_ENCODING = mM_MESSAGE_ENCODING;
	}
	public String getMessageBody() {
		return messageBody;
	}
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
	
	

}
