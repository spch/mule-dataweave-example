%dw 2.0
output application/dw

var attributes = readUrl('classpath://dw-help/rohan-source.json')
---
attributes groupBy $.awm_location mapObject ((value,key) -> using (	
	classificationId = value[0].classificationId,
	owningCompany = value[0].awm_owningcomp)
	{ 
		awmLocation: key,
		classificationId: classificationId,
		owningCompany: owningCompany,
		specs: value map ((item, index) -> {
			specName: item.recordName,
			specValue: item.recordValue,
			category: item.category
		})
    }
)